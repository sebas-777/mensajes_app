package org.example;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    public Connection get_connection(){
        Connection conection = null;
        try {
            Class.forName("org.postgresql.Driver");
            conection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/mensajes_app","postgres","isabella");

        }catch(Exception e){
            System.out.println(e);

        }
        return conection;
    }
}
