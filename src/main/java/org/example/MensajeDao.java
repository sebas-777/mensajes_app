package org.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajeDao {

    static String cantCreateMessage = "\n no se pudo crear el mensaje \n";
    static String createdMessage = "\n mensaje creado \n";

    static String cantDeletetMessages ="\n no se puedo eliminar el mensaje\n";

    static  String cantUpdateMessages = "\n no se pudo actualizar el mensaje ";

    static  String UpdateMessages = "\n se actualizo correctamente el mensaje";

    static String deleteMessage = "\n se elimino mensaje";

    public static void crearMensajeDB(Mensaje mensaje){
        Conexion db_connect = new Conexion();
        try (Connection conexion = db_connect.get_connection()){
            PreparedStatement ps = null;
            try {
                String query = "insert into mensajes (mensaje,autor_mensaje,fecha_mensaje) values(?,?,CURRENT_TIMESTAMP)";
                ps = conexion.prepareStatement(query);
                ps.setString(1,mensaje.getMensajes());
                ps.setString(2,mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println(createdMessage);

            }catch (SQLException e){
                System.out.println(e);
                System.out.println(cantCreateMessage);
            }

        }catch (Exception ex){
            System.out.println(ex);
        }
    }

    public static void leerMensajeDB(){
        Conexion db_connect = new Conexion();
        try (Connection conexion = db_connect.get_connection()){
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                String query ="select * from mensajes";
                ps = conexion.prepareStatement(query);
                rs = ps.executeQuery();

                while (rs.next()){
                    System.out.println("ID: " + rs.getInt("id_mensaje"));
                    System.out.println("Mensaje: " + rs.getString("mensaje"));
                    System.out.println("Autor Mensaje: " + rs.getString("autor_mensaje"));
                    System.out.println("Fecha de Menssaje: " + rs.getString("fecha_mensaje"));
                }
            }catch (SQLException e){
                System.out.println(e);
                System.out.println(cantUpdateMessages);
            }

        }catch (Exception ex){
            System.out.println(ex);

        }

    }

    public static void borrarMensajeDB(int id_mensaje){
        Conexion db_connect = new Conexion();
        try(Connection conexion = db_connect.get_connection()){
            PreparedStatement ps = null;
            try{
                String query = "delete from mensajes where id_mensaje = ?";
                ps=conexion.prepareStatement(query);
                ps.setInt(1,id_mensaje);
                ps.executeUpdate();
                System.out.println(deleteMessage);
            }catch (SQLException e){
                System.out.println(e);
                System.out.println(cantDeletetMessages);
            }
        }catch (Exception ex){
            System.out.println(ex);
        }


    }

    public static void actualizarMensajeDB(Mensaje mensaje){
        Conexion db_connect = new Conexion();
        try(Connection conexion = db_connect.get_connection()){
            PreparedStatement ps = null;
            try{
                String query ="UPDATE mensajes SET mensaje = ? WHERE id_mensaje= ?";
                ps = conexion.prepareStatement(query);
                ps.setString(1,mensaje.getMensajes());
                ps.setInt(2,mensaje.getId_mensaje());
                ps.executeUpdate();
                System.out.println(UpdateMessages );

            }catch(SQLException ex){
                System.out.println(ex);
                System.out.println(cantUpdateMessages);
            }
        }catch (SQLException e){
            System.out.println(e);
        }
    }

}
