package org.example;


import java.util.Scanner;

public class MensajeService {

    public  static void crearMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje ");
        String mensaje = sc.nextLine();

        System.out.println("cual es tu nombre:");
        String nombre = sc.nextLine();

        Mensaje registro = new Mensaje();
        registro.setMensajes(mensaje);
        registro.setAutor_mensaje(nombre);
        MensajeDao.crearMensajeDB(registro);

    }


    public static void listarMensaje(){
        MensajeDao.leerMensajeDB();

    }

    public static void borrarMensaje(){
        Scanner sc =  new Scanner(System.in);
        System.out.println("Indica el ID del mensaje a borrar");
        int id_mensaje = sc.nextInt();
        MensajeDao.borrarMensajeDB(id_mensaje);


    }

    public static void editarMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu nuevo mensaje :");
        String mensaje = sc.nextLine();

        System.out.println("Indica el ID del mensaje a editar");
        int id_mensaje = sc.nextInt();
        Mensaje actualizacion = new Mensaje();
        actualizacion.setId_mensaje(id_mensaje);
        actualizacion.setMensajes(mensaje);
        MensajeDao.actualizarMensajeDB(actualizacion);

    }
}
